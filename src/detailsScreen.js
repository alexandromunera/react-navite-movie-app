import React, { Component } from 'react';
import {  Text, ScrollView,View, Image,WebView,TouchableHighlight} from 'react-native';
import { AirbnbRating } from 'react-native-ratings';

import API from '../api';
import styles from './styles';

const makeHTML = (id) => {
  return (`
    <style>
      .video {
        position: relative;
        padding-bottom: 56.25%;
      }
      iframe {
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
      }
    </style>
    <div class="video">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/${id}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  `)
}

    class DetailsScreen extends Component {
        static navigationOptions = ({ navigation }) => {
          return {
            headerTitle: 'Movie details' ,
            headerRight: (
            <Image
              source={require('../assets/share.png')}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            ),
          };
        };
      
        state = {
        }

        async convertMinsToHrsMins(mins) { 
         let h = Math.floor(mins / 60);
         let m = mins % 60;
         h = h < 10 ? '0' + h : h;
         m = m < 10 ? '0' + m : m;
          return `${h}h:${m}m`;
       }

       async _getData(idFilm) {

          const detailMovieAPI = await API.getMovie(idFilm);
          const mainCastMovieAPI = await API.getMainCast(idFilm);
          const videosAPI = await API.getTrailer(idFilm);

          var shortOverview = detailMovieAPI.overview.substr(0,150) + '...';

          var genresConcate = detailMovieAPI.genres[0].name;

          if(detailMovieAPI.genres[1] !== undefined)
          {
            genresConcate = genresConcate + ', ' + detailMovieAPI.genres[1].name;
          }

          this.setState({
                title: detailMovieAPI.title,
                poster_path: detailMovieAPI.poster_path,
                runtime: await this.convertMinsToHrsMins(detailMovieAPI.runtime),
                genres: genresConcate,
                language: detailMovieAPI.spoken_languages[0].name,
                overview: shortOverview,
                largeOverview: detailMovieAPI.overview,
                vote_average: detailMovieAPI.vote_average,

                characterCast1: mainCastMovieAPI.cast[0].character,
                profilePhotoCast1: mainCastMovieAPI.cast[0].profile_path,
                nameCast1: mainCastMovieAPI.cast[0].name,
          
                characterCast2: mainCastMovieAPI.cast[1].character,
                profilePhotoCast2: mainCastMovieAPI.cast[1].profile_path,
                nameCast2: mainCastMovieAPI.cast[1].name,
          
                characterCast3: mainCastMovieAPI.cast[2].character,
                profilePhotoCast3: mainCastMovieAPI.cast[2].profile_path,
                nameCast3: mainCastMovieAPI.cast[2].name,

                keyTrailer: videosAPI[0].key,
              })

        }

        _onPressButtonYouTube() { 
          this.refs.scroll.scrollToEnd({animated: true,duration: 800});        
        }

        _onPressButtonReadMore(){
          this.setState({
            overview: this.state.largeOverview,
          })
        }

        componentWillMount = () => this._getData(this.props.navigation.getParam('MovieId', 'No-Id'))
    
        render() {
      
          const IMG_URL = 'https://image.tmdb.org/t/p/w780';
          const PROFIL_IMG_URL = 'https://image.tmdb.org/t/p/w45';

          return(
            <ScrollView ref="scroll" style={styles.containerDetailMovie}>
      
              <View style={styles.containerTrailer}>
                <Image style={styles.backdropPhotoMovie} 
                 source={{uri: IMG_URL + this.state.poster_path}}          
              /> 
      
                  <View style={styles.containerTitleInDetails}>
                  <Text style={styles.titleInDetails} >{this.state.title}</Text>
                  </View>
      
                  <View style={styles.containerStartsInDetails}>
                    <AirbnbRating
                      count={this.state.vote_average}
                      defaultRating={this.state.vote_average}
                      size={20}
                      isDisable={true}
                      showRating={false}
                    />          

                  </View>            

              <View style={styles.containerIconYouTube}>

                  <TouchableHighlight
                      onPress={ () => this._onPressButtonYouTube() }
                        underlayColor="white">
                        <Image
                          source={require('../assets/youtube.png')}
                          style={{ 
                            width: 50, 
                            height: 50 }}
                        />
                  </TouchableHighlight>               
                 
              </View>
            </View>
      
              <View style={styles.containerInfoDetail}>
                <View> 
                  <Text style={styles.titleText}>Duration</Text>
                  <Text style={styles.simpleText}>{this.state.runtime}</Text>
                </View>
                <View> 
                  <Text style={styles.titleText}>Genre</Text>
                  <Text style={styles.simpleText}>{this.state.genres}</Text>
                 </View>
      
                 <View> 
                  <Text style={styles.titleText}>Language</Text>
                  <Text style={styles.simpleText}>{this.state.language}</Text>
                 </View>           
              </View>
      
              <View style={styles.containerSypnosis}>
                <View> 
                  <Text style={styles.titleText}>Sypnosis</Text>
                  <Text style={styles.simpleText}>{this.state.overview}</Text>

                  <TouchableHighlight
                      onPress={ () => this._onPressButtonReadMore() }
                        underlayColor="white">
                      <Text style={styles.readMore}>Read more</Text>
                  </TouchableHighlight>
                </View>
              </View>
      
      
              <View style={styles.containerCast}>
                  <Text style={styles.titleText}>Main cast</Text>
                  <View style={styles.mainCast}>
      
                    <View style={styles.profileCast}>
                      <Text style={styles.simpleText}>{this.state.characterCast1}</Text>
                      
                      <Image style={styles.castPhoto} 
                          source={{uri: PROFIL_IMG_URL + this.state.profilePhotoCast1 }}/>
                          
                      <Text style={styles.simpleText}>{this.state.nameCast1}</Text>
                    </View>
      
                    <View style={styles.profileCast}>
                      <Text style={styles.simpleText}>{this.state.characterCast2}</Text>
                      
                      <Image style={styles.castPhoto} 
                          source={{uri: PROFIL_IMG_URL + this.state.profilePhotoCast2 }}/>
                          
                      <Text style={styles.simpleText}>{this.state.nameCast2}</Text>
                    </View>
      
                    <View style={styles.profileCast}>
                      <Text style={styles.simpleText}>{this.state.characterCast3}</Text>
                      
                      <Image style={styles.castPhoto} 
                          source={{uri: PROFIL_IMG_URL + this.state.profilePhotoCast3 }}/>
                          
                      <Text style={styles.simpleText}>{this.state.nameCast3}</Text>
                    </View>
      
                  </View>
              </View>

              <View style={styles.trailer}>
              <Text style={styles.titleText}>Trailer</Text>

                <WebView
                  source={{html: makeHTML(this.state.keyTrailer) }}
                />
              </View>
            </ScrollView>
          );
      
        } 
      }

      export default DetailsScreen;