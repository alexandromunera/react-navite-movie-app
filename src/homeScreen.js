import React, { Component } from 'react';
import {  FlatList,  View, Image, Text,TouchableHighlight} from 'react-native';

    import API from '../api';
    import styles from './styles';
    import Movie from './movie';    

class HomeScreen extends Component {
    static navigationOptions = ({ navigation }) => {
      return {
        headerTitle: 'Home',
        // headerRight: (
        // <Image
        //   source={require('../assets/filter.png')}
        //   style={{ width: 20, height: 20, marginRight: 10 }}
        // />
        // ),
      };
    };
  
    state = {
      popularMovies: [],
    }
  
   async componentWillMount() {
  
      const popularMoviesAPI = await API.getPopularMovies();
  
      this.setState({
        popularMovies: [
          popularMoviesAPI
        ],
      })
    }

    _onPressButtonView(){
      var numColumns = this.state.columnCount;

      if (numColumns == 2) {
        this.setState({
          columnCount: 1
        });
      }else{
        this.setState({
          columnCount: 2
        })
      }
    }
  
    render() {

      return (
        <View style={styles.container}>       
  
        <View style={styles.filtersContainer}>
                      <Text style={styles.filterText}>Most popular</Text>
                      <TouchableHighlight
                          onPress={ () => this._onPressButtonView() }
                          underlayColor="white">
                        <Image
                          source={require('../assets/view.png')}
                          style={{ width: 20, height: 20 }}
                        />
                      </TouchableHighlight>
          </View>
  
          <FlatList 
            ref="MovieFlatList"
            numColumns= {this.state.columnCount}     
            key= {this.state.columnCount}     
            data={this.state.popularMovies[0]}
            renderItem={({item}) =>  
                <Movie navigation={this.props.navigation} idMovie={item.id} title={item.title} year={item.release_date} language={item.original_language} rate={item.vote_average} classification={item.adult} imageUri={item.poster_path} />
            }  
            keyExtractor={(item, index) => item.id.toString()}
          />
          
        </View>
      );
    }
  }


  export default HomeScreen;