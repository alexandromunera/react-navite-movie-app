
import { StyleSheet} from 'react-native';

    const styles = StyleSheet.create({
        container: {
         flex: 1,
         paddingTop: 22,
        },
        item: {
          padding: 10,
          fontSize: 18,
          height: 44,
        },
      
        containerMovie: {
          margin: 8,
          flex:1,
          flexDirection: 'row',
        },
      
        simpleText: {
          color: 'grey'
        },
      
        titleText: {
          fontSize: 15,
          fontWeight: 'bold',
        },

        touchablePhotoMovie: {
          flex: 1,      
        },
      
        photoMovie: {
          flex: 1,
          marginRight: 20,
          height: 150,
          borderRadius:10,      
        },
      
        infoMovie:{
          flex:2,
          justifyContent: 'space-between',
        },
      
        filtersContainer: {
          
          flexDirection: 'row',
          justifyContent: 'space-between',
          margin: 10,
        },
        
        filterText: {
          fontSize: 20,
          fontWeight: 'bold',    
        },
      
        imgTest:{
          width: 400,
          height:400
        },
      
        backdropPhotoMovie:{
          flex: 1,
          height: 200,
      
        },
      
        containerDetailMovie: {
          flex:1,
          // justifyContent: 'flex-start',
        },
      
        containerTrailer: {
          flex:1,    
        },
      
        containerInfoDetail: {
          flex:1,
          paddingTop:10,
          paddingLeft:20,
          paddingRight:20,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        },
      
        containerSypnosis: {    
          flex:2,
          paddingLeft:20,
          paddingRight:20,
          paddingBottom:5,
          paddingTop:5,
        },

        containerCompleteSypnosis: {    
          flex:2,
          paddingLeft:20,
          paddingRight:20,
          paddingBottom:5,
          paddingTop:5,
        },
      
        containerCast: {
          flex:2,
          paddingLeft:20,
          paddingRight:20,
          paddingBottom:5,
        },
      
        mainCast:{
          flex:1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        },
      
        castPhoto:{
          width: 50,
          height: 50,
          borderRadius:50,
        },
      
        profileCast:{
          flex:1,
        },
      
        readMore:{
          color: 'red',
          textAlign: 'right',
        },
      
        containerTitleInDetails:{
            position: 'absolute',
            top: 0,
            left: 20,
            right: 0,
            bottom: 30,
            justifyContent: 'flex-end',
        },
      
        titleInDetails:{
          color: 'white',
          fontSize: 22,
          fontWeight: 'bold',
        },
      
        containerStartsInDetails:{
          position: 'absolute',
          top: 0,
          left: 20,
          right: 0,
          bottom: 10,
          justifyContent: 'flex-end',
          alignItems: 'flex-start'
        },

        containerIconYouTube:{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 20,
          bottom: -18,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        },

        inputSearchMovie: {
          padding: 15,
          fontSize: 15,
          borderWidth: 1,
          borderColor: '#eaeaea',
          borderRadius:10,
        },

        containerSearchMovie:{
          // marginTop: 20,
        },

        trailer: {
          height: 200,
          paddingLeft:20,
          paddingRight:20,
          paddingBottom:5,
        },

      })

    export default styles;