import React, { Component } from 'react';
import { Text, View, Image,
   TouchableHighlight} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';
import API from '../api';


class Movie extends Component{

        _onPressButton() { 

            this.props.navigation.navigate('Details',
                {
                  MovieId: this.props.idMovie.toString()
                }
            );          
        }

        state = {
        }

        async _getData(idFilm) {

          const detailMovieAPI = await API.getMovie(idFilm);

          var genresConcate = detailMovieAPI.genres[0] == undefined ? 'Not register' : detailMovieAPI.genres[0].name;

          if(detailMovieAPI.genres[1] !== undefined)
          {
            genresConcate = genresConcate + ', ' + detailMovieAPI.genres[1].name;
          }

          this.setState({
                genres: genresConcate                
              })
        }

        componentWillMount = () => this._getData(this.props.idMovie);

        render() {
      
          const IMG_URL = 'https://image.tmdb.org/t/p/w185';          
          
          return(
            <View style={styles.containerMovie}>
              <TouchableHighlight style={styles.touchablePhotoMovie}
                   onPress={ () => this._onPressButton() } >
               <Image style={styles.photoMovie} source={{uri: IMG_URL + this.props.imageUri}} /> 
              </TouchableHighlight>
              
              <View style={styles.infoMovie}>
               <View>
                <TouchableHighlight
                   onPress={ () => this._onPressButton() }
                    underlayColor="white">      
                  <Text style={styles.titleText}>{this.props.title}</Text>
                </TouchableHighlight>
      
                <Text style={styles.simpleText}>{this.props.year} | {this.props.language}</Text>
                  <Text style={styles.simpleText}>{this.state.genres}</Text>
                </View>
                <View>
                <Text style={styles.simpleText}>{this.props.rate}</Text>
                <Text style={styles.simpleText}>{this.props.classification ? 'Adult' : 'Public'}</Text>
                </View>
              </View>
            </View>
          );
        }
}

export default withNavigation(Movie);