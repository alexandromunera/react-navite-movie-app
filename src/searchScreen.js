import React, { Component } from 'react';
import {TextInput,View,Text,FlatList} from 'react-native';
import Search from 'react-native-search-box';

import API from '../api';
import styles from './styles';
import Movie from './movie';    


class SearchScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          headerTitle: 'Search'
        };
      };

  state = {
    text: '',
    movies: [],

  }

  handleSubmit = async () => {

    console.log("Entre a handleSumit");
    const movies = await API.searchMovie(this.state.text); 
    console.log(movies);

    this.setState({
        movies: [
            movies
        ],
      })
  }

  handleChangeText = (text) => {
    this.setState({
      text
    })
  }

  onSearch = async (searchText) => {

    const movies = await API.searchMovie(searchText); 
    console.log(movies);

    this.setState({
        movies: [
            movies
        ],
      })
}

  render() {
    return (
    <View style={styles.containerSearchMovie}>
        <Search
          ref="search_box"
          onSearch={this.onSearch}
          inputBorderRadius={10}
          placeholder='Type movie title'
          searchIconCollapsedMargin= {170}
          placeholderCollapsedMargin = {160}
        />

        <FlatList          
                data={this.state.movies[0]}
                renderItem={({item}) =>
                    <Movie navigation={this.props.navigation} idMovie={item.id} title={item.title} year={item.release_date} language={item.original_language}  generes='falta hacer'  rate={item.vote_average} classification={item.adult} imageUri={item.poster_path} />
                }
                keyExtractor={(item, index) => item.id.toString()}
            />
    </View>
    )
  }
}


export default SearchScreen;