import React, { Component } from 'react';
import { AppRegistry, Image} from 'react-native';
import { createAppContainer,createBottomTabNavigator, createStackNavigator, createSwitchNavigator} from 'react-navigation';

import DetailsScreen from './src/detailsScreen';
import MoreScreen from './src/moreScreen';
import SearchScreen from './src/searchScreen';
import HomeScreen from './src/homeScreen';
import Movie from './src/movie';

const getTabBarIcon = (navigation, focused, tintColor) => {
  const { routeName } = navigation.state;

  if (routeName === 'Home') {
    return (
      <Image
        source={require('./assets/home.png')}
        style={{width: 20, height: 20}}
      />
    );
        
  } else if (routeName === 'Search') {
    return (
      <Image
        source={require('./assets/search.png')}
        style={{width: 20, height: 20}}
      />
    );
  } else if (routeName === 'More') {
    return (
      <Image
        source={require('./assets/menu.png')}
        style={{width: 20, height: 20}}
      />
    );
  }

  return (
    <Image
      source={require('./assets/home.png')}
      style={{width: 20, height: 20}}
    />
  );
};

const Main = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen
  },
  {
    // initialRouteName: 'Home',
    navigationOptions: {
      // title: 'Un titulo genérico',
      headerTitleAllowFontScaling: true,
      headerBackTitle: 'Atras',
      gesturesEnabled: true,
      // headerBackImage: <Text>{`<=`}</Text>,
      // header: <Text style={{color: 'white'}}>esto es un header</Text>,
    },
    // initialRouteKey: 'login',
    // initialRouteParams: {
    //   nombre: 'Leonidas Esteban'
    // }
  }
);

const Search = createStackNavigator(
  {
    Search: SearchScreen
  },
  {
    // initialRouteName: 'Home',
    navigationOptions: {
      // title: 'Un titulo genérico',
      headerTitleAllowFontScaling: true,
      headerBackTitle: 'Atras',
      gesturesEnabled: true,
      // headerBackImage: <Text>{`<=`}</Text>,
      // header: <Text style={{color: 'white'}}>esto es un header</Text>,
    },
    // initialRouteKey: 'login',
    // initialRouteParams: {
    //   nombre: 'Leonidas Esteban'
    // }
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Main
    },
    Search: {
      screen: Search
    }, 
    More: {
      screen: MoreScreen
    } 
  },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) =>
          getTabBarIcon(navigation, focused, tintColor),
      }),
      tabBarOptions: {
        activeTintColor: 'black',
        activeBackgroundColor: '#F5F5F5',
        inactiveTintColor: 'gray',
      },
    }
);

const WithModal = createStackNavigator(
  {
    Main: {
      screen: TabNavigator
    },
    Movie: Movie,
  },
  {
    mode: 'modal',
    headerMode: 'none',
    cardStyle: {
      backgroundColor: 'white'
    },
    navigationOptions: {
      gesturesEnabled: true,
      tabBarVisible: false
    }
  }
)

const SwitchNavigator = createSwitchNavigator(
  {
    App: WithModal
  },
  {
    initialRouteName: 'App',
  }
)

const AppContainer = createAppContainer(SwitchNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}

AppRegistry.registerComponent('MovieApp', () => App);